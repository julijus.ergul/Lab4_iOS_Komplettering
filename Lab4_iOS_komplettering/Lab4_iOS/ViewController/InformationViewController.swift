//
//  InformationViewController.swift
//  Lab4_iOS
//
//  Created by Admin on 2018-07-30.
//  Copyright © 2018 Julijus Ergül. All rights reserved.
//

import UIKit

class InformationViewController: UIViewController {

    var name = " "
    var gender = " "
    var birthYear = " "
    var height = " "
    var mass = " "
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var birthYearLbl: UILabel!
    @IBOutlet weak var heightLbl: UILabel!
    @IBOutlet weak var massLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLbl.text = "Name: " + name
        genderLbl.text = "Gender: " + gender
        birthYearLbl.text = "Birth: " + birthYear
        heightLbl.text = "Height: " + height
        massLbl.text = "Mass: " + mass
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
