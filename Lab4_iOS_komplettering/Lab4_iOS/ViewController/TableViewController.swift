//
//  TableViewController.swift
//  Lab4_iOS
//
//  Created by Admin on 2018-07-30.
//  Copyright © 2018 Julijus Ergül. All rights reserved.
//

import UIKit

class TableViewController: UIViewController,UITableViewDataSource, UITableViewDelegate

{
    @IBOutlet weak var tableView: UITableView!
    var peopleArr = [People]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        getPeople()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    class People{
        var name:String = ""
        var gender:String = ""
        var birthYear:String = ""
        var height:String = ""
        var mass:String = ""
    }

    func getPeople(){
        let url = URL(string: "https://swapi.co/api/people/")
        let session = URLSession.shared
        
        let task = session.dataTask(with: url!) {(data, response, error) -> Void in
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String: AnyObject]
                let results:NSArray = json["results"] as! NSArray
                var x = 0
                while x < results.count {
                    let item:NSDictionary = results[x] as! NSDictionary
                    
                    let people = People()
                    people.name = item["name"] as! String
                    print(people.name)
                    people.gender = item["gender"] as! String
                    people.birthYear = item["birth_year"] as! String
                    people.height = item["height"] as! String
                    people.mass = item["mass"] as! String
                    
                    self.peopleArr.append(people)
                    x = x + 1
                    
                }
                DispatchQueue.main.async(){
                    self.tableView.reloadData()
                }
            }
            catch{
                print("Error with JSON:\(error)")
            }
            
        }
        task.resume()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return peopleArr.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InformationTableViewCell", for: indexPath) as! InformationTableViewCell
        cell.nameLabel.text = self.peopleArr[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toInformationSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toInformationSegue"{
            if let InformationViewController = segue.destination as? InformationViewController{
                InformationViewController.name = peopleArr[(tableView.indexPathForSelectedRow?.row)!].name
                InformationViewController.gender = peopleArr[(tableView.indexPathForSelectedRow?.row)!].gender
                InformationViewController.birthYear = peopleArr[(tableView.indexPathForSelectedRow?.row)!].birthYear
                InformationViewController.height = peopleArr[(tableView.indexPathForSelectedRow?.row)!].height
                InformationViewController.mass = peopleArr[(tableView.indexPathForSelectedRow?.row)!].mass
                
            }
        }
    }
    
}
