//
//  InformationTableViewCell.swift
//  Lab4_iOS
//
//  Created by Admin on 2018-07-30.
//  Copyright © 2018 Julijus Ergül. All rights reserved.
//

import UIKit

class InformationTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
